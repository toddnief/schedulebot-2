class AddClientNameToAcuityAppointments < ActiveRecord::Migration[5.1]
  def change
    add_column :acuity_appointments, :client_first_name, :string
    add_column :acuity_appointments, :client_last_name, :string
  end
end
