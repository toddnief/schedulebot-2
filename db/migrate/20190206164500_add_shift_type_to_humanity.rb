class AddShiftTypeToHumanity < ActiveRecord::Migration[5.1]
  def change
    add_column :humanity_shifts, :shift_type, :string
  end
end
