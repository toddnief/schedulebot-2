class AddAcuityIdToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :acuity_id, :string
  end
end
