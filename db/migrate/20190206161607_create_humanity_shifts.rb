class CreateHumanityShifts < ActiveRecord::Migration[5.1]
  def change
    create_table :humanity_shifts do |t|
      t.integer :shift_id
      t.string :shift_day
      t.string :formatted_date
      t.time :start_time
      t.time :end_time
      t.integer :humanity_employee_id
      t.string :employee_name

      t.timestamps
    end
  end
end
