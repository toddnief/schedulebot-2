class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :employee_name
      t.integer :humanity_id
      t.string :slack_id
      t.string :email

      t.timestamps
    end
  end
end
