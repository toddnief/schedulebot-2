class AddSlackChannelIdToEmployees < ActiveRecord::Migration[5.1]
  def change
  	add_column :employees, :slack_channel_id, :string
  end
end
