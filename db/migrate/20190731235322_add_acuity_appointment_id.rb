class AddAcuityAppointmentId < ActiveRecord::Migration[5.1]
  def change
    add_column :acuity_appointments, :appointment_id, :string
  end
end
