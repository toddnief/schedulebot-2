class AddFrontDeskEmployeeField < ActiveRecord::Migration[5.1]
  def change
        add_column :employees, :front_desk_employee, :boolean
  end
end
