class CreateAcuityAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :acuity_appointments do |t|
      t.string :appointment_type
      t.datetime :date
      t.datetime :start_time
      t.string :coach
      t.boolean :canceled
      t.integer :employee_id

      t.timestamps
    end
  end
end
