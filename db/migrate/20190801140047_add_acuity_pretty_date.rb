class AddAcuityPrettyDate < ActiveRecord::Migration[5.1]
  def change
    add_column :acuity_appointments, :pretty_date, :string
  end
end
