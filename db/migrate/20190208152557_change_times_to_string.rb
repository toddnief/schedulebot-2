class ChangeTimesToString < ActiveRecord::Migration[5.1]
  def change
  	  change_column :humanity_shifts, :start_time, :string
  	  change_column :humanity_shifts, :end_time, :string
  end
end
