class ChangeAcuityTimeToString < ActiveRecord::Migration[5.1]
  def change
    change_column :acuity_appointments, :start_time, :string
  end
end
