namespace :slack do
  desc "pull slack employees and create associated channels"
  task pull: :environment do
    SlackPuller.new.pull_slack_employees
    SlackPuller.new.add_slack_channel_id
  end

  desc "actually send schedule messages in Slack"
  task send_schedule_messages: :environment do
    SlackSender.send_all_schedules
  end
end
