namespace :humanity do
  desc "delete all humanity records and re-import"
  task pull: :environment do
    HumanityShift.delete_all
    HumanityPuller.new.employees
    HumanityPuller.new.shifts
  end
end
