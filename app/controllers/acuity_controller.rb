class AcuityController < ApplicationController
  protect_from_forgery with: :null_session

  def route_acuity_orders
    parsed_request = AcuityPuller.new.get_acuity_order_details
    if parsed_request["products"][0]["id"] == 153446
      create_zen_planner_profile(parsed_request)
    end
  end

  def route_acuity_appointments
    response = AcuityPuller.new.get_acuity_appointment_details(params)
    AcuityAdder.new.add_acuity_appointment_to_database(response)
    SlackSender.send_new_appointment_message(response)
    if response["appointmentTypeID"] == 405820 || response["appointmentTypeID"] == 5307264
      create_zen_planner_profile(response)
    end
  end

  def acuity_appointment_canceled
    response = AcuityPuller.new.get_acuity_appointment_details(params)
    AcuityAdder.new.update_canceled_appointments(response)
    SlackSender.send_canceled_appointment_message(response)
  end

  def acuity_appointment_rescheduled
    response = AcuityPuller.new.get_acuity_appointment_details(params)
    AcuityAdder.new.update_rescheduled_appointments(response)
    SlackSender.send_rescheduled_appointment_message(response)
  end

  def send_acuity_appointments
    AcuityPuller.new.add_acuity_id_to_employees
    SlackSender.send_acuity_appointments
  end

  def create_zen_planner_profile(parsed_request)
    # configure the driver to run in headless mode
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options

    # navigate driver to Zen Planner form and fill it out with information from Acuity appointment
    driver.navigate.to "http://southlooptools.com/zen-planner-form/zen-planner-form.html"

    element = nil
    wait = Selenium::WebDriver::Wait.new(timeout: 10) # seconds
    wait.until { element = driver.find_element(:id,"47883e4d-3efc-49f4-88ca-3d2d5265e86a")}

    driver.switch_to.frame(element)

    wait.until { element = driver.find_element(:xpath,'//*[@id="first-name"]') }

    driver.find_element(:xpath,'//*[@id="first-name"]').send_keys "#{parsed_request["firstName"]}"
    driver.find_element(:xpath,'//*[@id="last-name"]').send_keys "#{parsed_request["lastName"]}"
    driver.find_element(:xpath,'//*[@id="email"]').send_keys "#{parsed_request["email"]}"
    driver.find_element(:xpath,'//*[@id="phone"]').send_keys "#{parsed_request["phone"]}"
    button = driver.find_element(:xpath,'//*[@id="lead-capture-widget"]/div[1]/div[5]/div[2]/button').click

    sleep 5

    driver.quit
  end

  def show
    @appointments = AcuityAppointment.all
  end

  def show_next_seven_days
    @appointments_next_seven_days = AcuityAppointment.where('date >= ? AND date <= ?', Date.today, Date.today + 7.days)
  end

end
