class HumanityController < ApplicationController
  def index
  end

  def pull_humanity_employees
    HumanityPuller.new.employees
    AcuityPuller.new.add_acuity_id_to_employees
  end

  def pull_humanity_shifts(options = {})
    HumanityPuller.new.shifts
  end

  def clear_humanity_shifts
    HumanityShift.delete_all
  end

  def delete_all_employees
    Employee.delete_all
  end

  def clear_and_pull_humanity_shifts
    clear_humanity_shifts
    pull_humanity_shifts(options = {})
    HumanityPuller.new.sort_front_desk_employees
  end

  def clear_and_pull_humanity_employees
    delete_all_employees
    pull_humanity_employees
  end

  def pull_humanity_employees_and_shifts
    clear_and_pull_humanity_employees
    clear_and_pull_humanity_shifts
    flash[:success] = "Humanity Employees & Shifts Pulled"
    redirect_to '/schedulebot'
  end

  def show
    @shifts = HumanityShift.all
  end

end
