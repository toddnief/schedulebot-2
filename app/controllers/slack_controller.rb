class SlackController < ApplicationController
  def index
  end

  def send_all_schedules
    SlackSender.send_all_schedules
    flash[:success] = "Slack Messages Sent"
    redirect_to '/schedulebot'
  end

  def slack_employees_to_database
    parsed_slack_employees = SlackPuller.new.pull_slack_employees
    SlackPuller.new.add_slack_employees_to_database(parsed_slack_employees)
    SlackPuller.new.add_slack_channel_id
  end

  def add_channel_and_send_messages
    slack_employees_to_database
    send_all_schedules
  end

  def send_front_desk_schedules
    slack_employees_to_database
    SlackSender.send_front_desk_schedules
    flash[:success] = "Front Desk Schedules Sent"
    redirect_to '/schedulebot'
  end

  def send_non_front_desk_schedules
    slack_employees_to_database
    SlackSender.send_non_front_desk_schedules
    flash[:success] = "Non-Front Desk Schedules Sent"
    redirect_to '/schedulebot'
  end

  def send_empty_shifts_message
    SlackSender.send_empty_shifts_message
    flash[:success] = "Empty Shifts Message Sent"
    redirect_to '/schedulebot'
  end

end
