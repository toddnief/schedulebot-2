class SlackSender
  attr_reader :token

  @@schedule_master_id = "U022RPDV35W"
  @@todd_id = "U282HNC9L"

  @@front_desk_shifts_channel = "C6JNWGSNL"
  @@coaching_shifts_channel = "C6KAJ41L3"

  # testing channels
  # @@front_desk_shifts_channel = "C4N8JEH2R"
  # @@coaching_shifts_channel = "C4N8JEH2R"

  def initialize(token)
    @token = token
  end

  def create_schedule_message(employee)
    return unless employee.upcoming_shifts.present?

    schedule_message = []
    schedule_message << "Hey #{ employee.employee_name },"
    schedule_message << "Here's your schedule for this coming week."
    schedule_message << "*Please let <@#{@@schedule_master_id}> know immediately (in a separate message — don't reply here) if there are any issues with your schedule for this week.* \n\nSchedulebot sends out the schedule as listed in Humanity, so we need to know right away if something is incorrect."
    schedule_message << "\nCheck your schedule in the Humanity app for the most current schedule. _You will not receive a new schedulebot message if you are added or removed from a shift_."

    employee.upcoming_shifts.group_by(&:pretty_date).each do |day, shifts_on_specific_day|
      schedule_message << "*#{ day }*"
      schedule_message += shifts_on_specific_day.map(&:pretty_time_and_date)
    end

    schedule_message.join("\n")
  end

  def create_empty_shifts
    # maybe need to create an "empty shifts employee"

  end

  def send_slack_message(slack_channel_id, schedule_message)
    # testing channel
    # slack_channel_id = "C4N8JEH2R"

    url = URI("https://slack.com/api/chat.postMessage")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/x-www-form-urlencoded'
    request["cache-control"] = 'no-cache'
    request.body = "token=#{token}&text=#{schedule_message}&channel=#{slack_channel_id}"
    response = http.request(request)
  end

  def send_schedule(employee)
    schedule_message = create_schedule_message(employee)
    send_slack_message(employee.slack_channel_id, schedule_message)
  end

  def send_confirmation_message(slack_channel_id)
    confirmation_message = "Schedulebot was just sent out. Check your DMs, and please let <@#{@@todd_id}> know if you didn't receive a message and you are scheduled to work this coming week. \n\nLet <@#{@@schedule_master_id}> know if there are any issues with your schedule for this week ASAP."
    send_slack_message(slack_channel_id, confirmation_message)
  end

  def self.send_empty_shifts_message
    sender = self.new(ENV["SLACK_TOKEN"])

    empty_shifts = HumanityShift.where(:employee_name => "(Empty Shift)")

    # binding.pry
    
    if empty_shifts.length != 0
      empty_shifts_message = []
      empty_shifts_message << "Here's the empty shifts for this coming week."

      empty_shifts.group_by(&:pretty_date).each do |day, shifts_on_specific_day|
        empty_shifts_message << "*#{ day }*"
        empty_shifts_message += shifts_on_specific_day.map(&:pretty_time_and_date)
      end
      empty_shifts_message = empty_shifts_message.join("\n")
    else
      empty_shifts_message = "This is Schedulebot speaking. There are no empty shifts this week."
    end

    # binding.pry

    sender.send_slack_message(@@front_desk_shifts_channel, empty_shifts_message)
    sender.send_slack_message(@@coaching_shifts_channel, empty_shifts_message)
  end

  def self.send_all_schedules
    sender = self.new(ENV["SLACK_TOKEN"])
    Employee.all.each do |employee|
      sender.send_schedule(employee) if employee.humanity_id.present?
    end
  end

  def self.send_front_desk_schedules
    # front_desk_shifts_channel = "C6JNWGSNL"
    sender = self.new(ENV["SLACK_TOKEN"])
    Employee.all.each do |employee|
      sender.send_schedule(employee) if employee.humanity_id.present? && employee.front_desk_employee
    end
    sender.send_confirmation_message(@@front_desk_shifts_channel)
  end

  def self.send_non_front_desk_schedules
    # coaching_shifts_channel = "C6KAJ41L3"
    sender = self.new(ENV["SLACK_TOKEN"])
    Employee.all.each do |employee|
      sender.send_schedule(employee) if employee.humanity_id.present? && (employee.front_desk_employee != true)
    end
    sender.send_confirmation_message(@@coaching_shifts_channel)
  end

  def self.send_acuity_appointments
    sender = self.new(ENV["SLACK_TOKEN"])
    Employee.all.each do |employee|
      if employee.appointments.where('date >= ? AND date <= ?', Date.today, Date.today + 7.days).present?
        sender.send_acuity_schedule(employee)
      end
    end
  end

  def send_acuity_schedule(employee)
    appointments_message = create_appointments_message(employee)
    send_acuity_message(employee.slack_channel_id, appointments_message)
  end

  def create_appointments_message(employee)
    appointments_message = AcuityPuller.new.create_acuity_message(employee)
    appointments_message
  end

  def send_acuity_message(slack_channel_id, appointment_message)
    #testing channel
    slack_channel_id = "C4N8JEH2R"

    url = URI("https://slack.com/api/chat.postMessage")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/x-www-form-urlencoded'
    request["cache-control"] = 'no-cache'
    request.body = "token=#{token}&text=#{appointment_message}&channel=#{slack_channel_id}"
    response = http.request(request)
  end

  def self.send_new_appointment_message(appointment)
    sender = self.new(ENV["SLACK_TOKEN"])

    new_appointment_message = []
    new_appointment_message << "Hey #{appointment["calendar"]},"
    new_appointment_message << "You've just been scheduled with #{appointment["firstName"]} #{appointment["lastName"]} for #{appointment["type"]} on #{DateTime.strptime(appointment["datetime"]).strftime('%a, %b %d')} at #{appointment["time"]}"
    new_appointment_message = new_appointment_message.join("\n")

    slack_channel_id = Employee.where(acuity_id: appointment["calendarID"]).first.slack_channel_id

    #testing channel
    #slack_channel_id = "C4N8JEH2R"

    sender.send_slack_message(slack_channel_id,new_appointment_message)
  end

  def self.send_canceled_appointment_message(appointment)
    sender = self.new(ENV["SLACK_TOKEN"])

    canceled_appointment_message = []
    canceled_appointment_message << "Hey #{appointment["calendar"]},"
    canceled_appointment_message << "Your appointment with #{appointment["firstName"]} #{appointment["lastName"]} for #{appointment["type"]} on #{DateTime.strptime(appointment["datetime"]).strftime('%a, %b %d')} at #{appointment["time"]} has been canceled."
    canceled_appointment_message = canceled_appointment_message.join("\n")

    slack_channel_id = Employee.where(acuity_id: appointment["calendarID"]).first.slack_channel_id

    #testing channel
    #slack_channel_id = "C4N8JEH2R"

    sender.send_slack_message(slack_channel_id,canceled_appointment_message)
  end

  def self.send_rescheduled_appointment_message(appointment)
    sender = self.new(ENV["SLACK_TOKEN"])

    binding.pry

    rescheduled_appointment_message_old_coach = []
    rescheduled_appointment_message_old_coach << "Hey #{appointment["calendar"]},"
    rescheduled_appointment_appointment_message_old_coach << "Your appointment with #{appointment["firstName"]} #{appointment["lastName"]} for #{appointment["type"]} on #{DateTime.strptime(appointment["datetime"]).strftime('%a, %b %d')}, at #{appointment["time"]} has been canceled."
    rescheduled_appointment_message_old_coach = canceled_appointment_message.join("\n")

    slack_channel_id = Employee.where(acuity_id: appointment["calendarID"]).first.slack_channel_id

    #testing channel
    slack_channel_id = "C4N8JEH2R"

    sender.send_slack_message(slack_channel_id,rescheduled_appointment_message_old_coach)
  end
end
