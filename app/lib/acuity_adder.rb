  # send slack message

  # handle canceled appointments

  # handle rescheduled appointments
class AcuityAdder
  def add_acuity_appointment_to_database(response)
    AcuityAppointment.create_from_json!(response)
  end

  def update_rescheduled_appointments(response)
    AcuityAppointment.find_by(appointment_id: response["id"]).reschedule_appointment(response)
  end

  def update_canceled_appointments(response)
    AcuityAppointment.find_by(appointment_id: response["id"]).cancel_appointment(response)
  end
end
