class AcuityPuller
  def get_acuity_employees
    acuity_user_id = 11518015
    acuity_api_key = ENV["ACUITY_API_KEY"]

    uri = URI.parse("https://acuityscheduling.com/api/v1/calendars")
    request = Net::HTTP::Get.new(uri)
    request.basic_auth("#{acuity_user_id}", "#{acuity_api_key}")

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    parsed_acuity_employees = JSON.parse(response.body)
  end

  #add acuity ID to employees
  def add_acuity_id_to_employees
    parsed_acuity_employees = get_acuity_employees

    parsed_acuity_employees.each do |emp_data|

      email = emp_data["email"].downcase
        acuity_id = emp_data["id"]

      if email.present?

          employee = Employee.where(email: email).first

        #add employees to database
          if employee && !employee.acuity_id.present?
              employee.update(acuity_id: acuity_id)
          elsif employee
              #do nothing
          else
              Employee.create(email: email, acuity_id: acuity_id)
          end
      end

    end
  end

  def get_acuity_appointment_details(params)
    #send API request to Acuity to get information for appointment based upon appointment ID
    acuity_user_id = 11518015
    acuity_api_key = ENV["ACUITY_API_KEY"]

    uri = URI.parse("https://acuityscheduling.com/api/v1/appointments/#{params['id']}")
    request = Net::HTTP::Get.new(uri)
    request.basic_auth("#{acuity_user_id}", "#{acuity_api_key}")

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    parsed_request = JSON.parse(response.body)
  end

  def get_acuity_order_details
    #send API request to Acuity to get information for appointment based upon appointment ID
    acuity_user_id = 11518015
    acuity_api_key = ENV["ACUITY_API_KEY"]

    uri = URI.parse("https://acuityscheduling.com/api/v1/orders/#{params['id']}")
    request = Net::HTTP::Get.new(uri)
    request.basic_auth("#{acuity_user_id}", "#{acuity_api_key}")

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    parsed_request = JSON.parse(response.body)
  end

  def create_acuity_message(employee)
    acuity_appointments_message = []
    acuity_appointments_message << "Hey #{ employee.employee_name },"
    acuity_appointments_message << "Here's your appointments this coming week."

    upcoming_appointments_with_canceled = employee.appointments.where('date >= ? AND date <= ?', Date.today, Date.today + 7.days)
    upcoming_appointments = upcoming_appointments_with_canceled.where(canceled: [false,nil])

    upcoming_appointments.group_by(&:pretty_date).each do |day, appointments_on_specific_day|
      acuity_appointments_message << "*#{ day }*"
      appointments_on_specific_day.each do |appointment|
        acuity_appointments_message << "#{appointment.start_time}: #{appointment.appointment_type} with #{appointment.client_first_name} #{appointment.client_last_name}"
      end
    end
    acuity_appointments_message = acuity_appointments_message.join("\n")
  end
end
