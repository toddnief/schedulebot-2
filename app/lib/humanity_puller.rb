class HumanityPuller
  def employees
    save_employees_to_database(parsed_employees_response)
  end

  def shifts
    save_shifts_to_database(parsed_humanity_shifts)
  end

  def sort_front_desk_employees
    Employee.all.each do |employee|
      employee.upcoming_shifts.each do |shift|
        if shift.shift_type == "Front Desk"
          employee.front_desk_employee = true
          employee.save
        end
      end
    end
  end

  private

  def save_shifts_to_database(response)
    response["data"].each do |shift_data|
      HumanityShift.create_from_json!(shift_data)
    end
  end

  def save_employees_to_database(response)
    response["data"].each do |employee_data|
      email = employee_data["email"]
      if email.present?
        email = email.downcase
      end

      employee_name = employee_data["name"]
      humanity_id = employee_data["id"]

      Employee.find_or_create_by(email: email) do |employee|
        employee.employee_name = employee_name
        employee.humanity_id = humanity_id
      end
    end
  end

  def parsed_employees_response
    url = URI("https://www.humanity.com/api/v2/employees?access_token=#{ access_token }")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(url)
    request["content-type"] = 'application/x-www-form-urlencoded'
    request["cache-control"] = 'no-cache'

    response = http.request(request)

    parsed_employees_response = JSON.parse(response.body)
  end

  def parsed_humanity_shifts
    start_date = Date.today
    end_date = Date.today + 7

    # pull shifts from date range
    url = URI("https://www.humanity.com/api/v2/shifts?access_token=#{ access_token }&start_date=#{ start_date }&end_date=#{ end_date }")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(url)
    request["content-type"] = 'application/x-www-form-urlencoded'

    response = http.request(request)

    JSON.parse(response.body)
  end

  def access_token
    url = URI("https://www.humanity.com/oauth2/token.php")

    humanity_client_id = ENV["HUMANITY_ID"]
    humanity_client_secret = ENV["HUMANITY_SECRET"]
    humanity_username = ENV["HUMANITY_USERNAME"]
    humanity_password = ENV["HUMANITY_PASSWORD"]

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/x-www-form-urlencoded'
    request["cache-control"] = 'no-cache'
    request["postman-token"] = '4927bff0-0cf8-d2be-8a7a-33bf5a850052'
    request.body = "client_id=#{humanity_client_id}&client_secret=#{humanity_client_secret}&grant_type=password&username=#{humanity_username}&password=#{humanity_password}"

    response = http.request(request)

    parsed_response = JSON.parse(response.body)

    parsed_response["access_token"]
  end
end
