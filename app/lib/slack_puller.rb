class SlackPuller
  def pull_slack_employees
    token = ENV["SLACK_TOKEN"]

    uri = URI.parse("https://slack.com/api/users.list?token=#{token}")
    response = Net::HTTP.get_response(uri)

    parsed_slack_employees = JSON.parse(response.body)
  end

  def add_slack_employees_to_database(parsed_slack_employees)
    parsed_slack_employees["members"].each do |emp_data|

      email = emp_data["profile"]["email"]

      if email.present?
        email = email.downcase
      end

      slack_id = emp_data["id"]

      if emp_data["deleted"] == false && email.present?

          employee = Employee.where(email: email).first

        #add employees to database
          if employee && !employee.slack_id.present?
              employee.update(slack_id: slack_id)
          elsif employee
              #do nothing
          else
              Employee.create(email: email, slack_id: slack_id)
          end
      end

    end
  end

  def add_slack_channel_id
    Employee.all.each(&:create_slack_channel)
  end
end
