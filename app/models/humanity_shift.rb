# t.integer "shift_id"
# t.string "shift_day"
# t.string "formatted_date"
# t.string "start_time"
# t.string "end_time"
# t.integer "humanity_employee_id"
# t.string "employee_name"
# t.datetime "created_at", null: false
# t.datetime "updated_at", null: false
# t.string "shift_type"
# t.string "pretty_date"

class HumanityShift < ApplicationRecord

  has_one :employee, foreign_key: :humanity_id, primary_key: :humanity_employee_id

  def self.create_from_json!(attributes)
    shift_type = attributes["schedule_name"]
    formatted_date = attributes["start_date"]["formatted"]
    start_time = attributes["start_date"]["time"]
    end_time = attributes["end_date"]["time"]
    shift_id = attributes["id"]
    shift_day = attributes["start_date"]["weekday"]
    pretty_date = shift_day + ', ' + formatted_date

    if !attributes["employees"].nil?
      attributes["employees"].each do |emp|
        employee_name = emp["name"]
        humanity_employee_id = emp["id"]

        # add to database
        create(shift_type: shift_type, formatted_date: formatted_date, start_time: start_time, end_time: end_time, shift_day: shift_day, shift_id: shift_id, employee_name: employee_name, humanity_employee_id: humanity_employee_id, pretty_date: pretty_date)
      end
      else
        employee_name = "(Empty Shift)"
        humanity_employee_id = 0

        # add to database
        create(shift_type: shift_type, formatted_date: formatted_date, start_time: start_time, end_time: end_time, shift_day: shift_day, shift_id: shift_id, employee_name: employee_name, humanity_employee_id: humanity_employee_id, pretty_date: pretty_date)
      end
  end

  def pretty_time_and_date
    "#{ shift_type } #{ start_time }-#{ end_time }"
  end
end
