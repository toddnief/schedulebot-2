# t.string "appointment_type"
# t.datetime "date"
# t.datetime "start_time"
# t.string "coach"
# t.boolean "canceled"
# t.integer "employee_id"
# t.string "client_first_name"
# t.string "client_last_name"
# t.string "pretty_date"

class AcuityAppointment < ApplicationRecord

  validates :appointment_id, uniqueness: true, on: :create

  #need to figure this out and associate calendarID with an employee
  has_one :employee, foreign_key: :acuity_id, primary_key: :employee_id

  # #should this all be "attributes"?
  def self.create_from_json!(parsed_response)
    appointment_type = parsed_response["type"]
    date = parsed_response["date"]
    start_time = parsed_response["time"]
    coach = parsed_response["calendar"]
    employee_id = parsed_response["calendarID"]
    client_first_name = parsed_response["firstName"]
    client_last_name = parsed_response["lastName"]
    appointment_id = parsed_response["id"]
    pretty_date = parsed_response["date"]

    # add to database
    create(
      appointment_type: appointment_type,
      date: date,
      start_time: start_time,
      coach: coach,
      employee_id: employee_id,
      client_first_name: client_first_name,
      client_last_name: client_last_name,
      appointment_id: appointment_id,
      pretty_date: pretty_date)
  end

  def reschedule_appointment(response)
    appointment_type = response["type"]
    date = response["date"]
    start_time = response["time"]
    coach = response["calendar"]

    appointment = self
    appointment.update(
      appointment_type: appointment_type,
      date: date,
      start_time: start_time,
      coach: coach
      )
  end

  def cancel_appointment(response)
    appointment = self
    appointment.update(canceled: true)
  end

end
