# t.string   "employee_name"
# t.integer  "humanity_id"
# t.string   "slack_id"
# t.string   "email"
# t.datetime "created_at", null: false
# t.datetime "updated_at", null: false
# t.string   "slack_channel_id"

class Employee < ApplicationRecord
  has_many :upcoming_shifts,
    foreign_key: :humanity_employee_id,
    primary_key: :humanity_id,
    class_name: "HumanityShift"

   has_many :appointments,
     foreign_key: :employee_id,
     primary_key: :acuity_id,
     class_name: "AcuityAppointment"

  def create_slack_channel
    employee = self

    return unless employee.slack_id?
    return if employee.slack_channel_id?

    url = URI("https://slack.com/api/conversations.open")
    token = ENV["SLACK_TOKEN"]

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    request["content"] = 'application/x-www-form-urlencoded'
    request["cache-control"] = 'no-cache'
    request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"token\"\r\n\r\n#{token}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"users\"\r\n\r\n#{employee.slack_id}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    response = http.request(request)

    parsed_slack_channel_response = JSON.parse(response.body)
    #binding.pry
    slack_channel_id = parsed_slack_channel_response["channel"]["id"]

    employee.update(slack_channel_id: slack_channel_id)
  end
end
