Rails.application.routes.draw do

  #views
  get '/schedulebot', to: 'humanity#index'

  get '/schedulebot/shifts', to: 'humanity#show'

  get '/schedulebot/employees', to: 'employees#show'

  get '/schedulebot/appointments', to: 'acuity#show'

  get '/schedulebot/appointments-next-week', to: 'acuity#show_next_seven_days'

  #Pull info from Humanity to Database
  post 'humanity/pull_humanity_employees_and_shifts', to: 'humanity#pull_humanity_employees_and_shifts'

  #Send schedules
  post 'slack/send_front_desk_schedules', to: 'slack#send_front_desk_schedules'

  post 'slack/send_non_front_desk_schedules', to: 'slack#send_non_front_desk_schedules'

  post 'acuity/send-acuity-appointments', to: "acuity#send_acuity_appointments"

  post 'slack/send-empty-shifts', to: 'slack#send_empty_shifts_message'

  #Acuity webhooks
  post 'acuity/acuity-appointments', to: "acuity#route_acuity_appointments"

  post 'acuity/acuity-orders', to: "acuity#route_acuity_orders"

  post 'acuity/acuity-appointment-rescheduled', to: "acuity#acuity_appointment_rescheduled"

  post 'acuity/acuity-appointment-canceled', to: "acuity#acuity_appointment_canceled"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
